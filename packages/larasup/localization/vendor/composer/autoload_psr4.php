<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(__DIR__);
$baseDir = dirname($vendorDir);

return array(
    'Larapacks\\Localization\\' => array($baseDir . '/src'),
);
