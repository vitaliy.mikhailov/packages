<?php

namespace Larasup\Localization;

use Illuminate\Support\ServiceProvider;

class LocalizationDomainProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->loadMigrationsFrom(__DIR__ . '/../Migrations/');
    }
}
