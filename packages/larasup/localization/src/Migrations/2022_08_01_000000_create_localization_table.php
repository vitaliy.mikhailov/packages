<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('localizations', function (Blueprint $table) {
            $table->id()->comment('Идентификатор');
            $table->string('object_class')->comment('Класс');
            $table->integer('model_primary_value')->comment('Идентификатор сущности');
            $table->string('language_code', 2)->default('ru')->comment('Язык');
            $table->text('key')->comment('Ключ');
            $table->text('value')->comment('Значение');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('localizations');
    }

};
