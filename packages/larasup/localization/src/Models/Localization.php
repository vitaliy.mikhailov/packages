<?php

namespace Larasup\Localization\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Item
 * @package App\Modes\System
 *
 * @property $id
 * @property $class_name
 * @property $model_primary_value
 * @property $language_code
 * @property $key
 * @property $value
 * @property $status
 */
class Localization extends Model
{
    protected $table = 'localizations';

    protected $fillable = ['class_name', 'model_primary_value', 'language_code', 'key', 'value', 'status'];

    public $timestamps = false;
}
