<?php

namespace Larasup\Localization\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Larasup\Localization\Models\Localization;
use Exception;

/**
 * @property string $primaryKey
 */
trait Localize
{
    private array $localizeData = [];

    public function getLocalization($key, null|string $language = null): mixed
    {
        $this->validateLocalizationBeforeWork($key);

        if (
            empty($this->localizeData)
            || (!is_null($language) && $language != app()->getLocale())
        ) {
            $this->localizeData = $this->getLocalizationBulk($language);
        }

        if (!isset($this->localizeData[$key])) {
            $primaryKeyName = $this->primaryKey;
            $errorMessage = 'Localization not found for field "' . $key .
                '". (Additional data: Model primary value: "' . $this->$primaryKeyName .
                '", Model: "' . $this->getMorphName() . '")';
            throw new Exception($errorMessage);
        }

        return $this->localizeData[$key];
    }

    public function getLocalizationBulk(null|string $language = null): array
    {
        return $this->localization()
            ->where('language_code', $language ?? app()->getLocale())
            ->get()
            ->pluck('value', 'key')
            ->toArray();
    }

    public function setLocalization($key, $value, null|string $language = null): void
    {
        $this->validateLocalizationBeforeWork($key);

        $objectName = $this->getMorphName();
        $primaryKey = $this->primaryKey;
        $systemLanguage = app()->getLocale();

        Localization::query()
            ->updateOrCreate(
                [
                    'object_class' => $objectName,
                    'model_primary_value' => $this->$primaryKey,
                    'language_code' => $language ?? $systemLanguage,
                    'key' => $key
                ],
                [
                    'value' => $value
                ]
            );

        if (is_null($language) || $language == $systemLanguage) {
            $this->localizeData[$key] = $value;
        }
    }

    public function setLocalizationBulk(array $data, null|string $language = null): void
    {
        foreach ($data as $key => $value) {
            $this->setLocalization($key, $value, $language);
        }
    }

    private function validateLocalizationBeforeWork($key): void
    {
        if (isset($this->localize) && !in_array($key, $this->localize)) {
            throw new Exception("Field {$key} is not in localize array");
        }

        $primaryKeyName = $this->primaryKey;
        if (is_null($this->$primaryKeyName)) {
            throw new Exception("Model doesn't have primary key");
        }
    }

    private function getMorphName(): string
    {
        try {
            /** @var $this Model */
            return $this->getMorphClass();
        } catch (Exception) {
            return get_class($this);
        }
    }

    public function localization(): HasMany
    {
        /** @var $this Model */
        return $this->hasMany(Localization::class, 'model_primary_value')
            ->where('object_class', $this->getMorphName());
    }
}
