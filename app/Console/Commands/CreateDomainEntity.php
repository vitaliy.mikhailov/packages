<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CreateDomainEntity extends Command
{
    const APPLICATIONS = 'Applications';
    const DOMAINS = 'Domains';
    const CONTRACTS = 'Contracts';

    const MODELS = 'Models';
    const SERVICES = 'Services';
    const FILTERS = 'Filters';

    protected $signature = 'ddd:createEntity {path}';

    protected $description = 'Create model and layers: controller application and service';

    public function handle()
    {
        $path = $this->argument('path');

        if (substr_count($path, DIRECTORY_SEPARATOR) !== 1) {
            $this->error('The path pattern should be *DomainName*' . DIRECTORY_SEPARATOR . '*EntityName*');
        }

        list($domainName, $entityName) = explode(DIRECTORY_SEPARATOR, $path);

//        $this->makeMigration($entityName);
        $this->makeModel($domainName, $entityName);
        $this->makeDomainInfrastructure($domainName);
        $this->makeApplication($domainName);

        return 0;
    }

    private function makeModel(string $domainName, string $entityName): void
    {
        $path = ['app', self::DOMAINS, $domainName, self::MODELS, $entityName];
        $path = implode(DIRECTORY_SEPARATOR, $path) . '.php';
        dd($path);
        if (!Storage::disk('base')->exists($path)) {
            $preset = $this->getSource(self::APPLICATIONS);
            $preset = Str::replace('{$domainName}', $domainName, $preset);
            Storage::disk('base')->put($path, $preset);
        }
    }

    private function getSource($type)
    {
        $paths = [
            self::APPLICATIONS => [__DIR__, 'Sources', 'Classes', 'Application.txt']
        ];

        return file_get_contents(implode(DIRECTORY_SEPARATOR, $paths[$type]));
    }

    private function makeDomainInfrastructure(string $domainName): void
    {
        $paths = [
            [self::APPLICATIONS],
            [self::CONTRACTS],
            [self::CONTRACTS, $domainName],
            [self::DOMAINS],
            [self::DOMAINS, $domainName],
            [self::DOMAINS, $domainName, self::MODELS],
            [self::DOMAINS, $domainName, self::SERVICES],
            [self::DOMAINS, $domainName, self::FILTERS]
        ];

        foreach ($paths as $path) {
            $path = app_path(implode(DIRECTORY_SEPARATOR, $path));
            if (!file_exists($path)) mkdir($path);
        }
    }

    private function makeApplication(string $domainName): void
    {
        $path = ['app', self::APPLICATIONS, $domainName];
        $path = implode(DIRECTORY_SEPARATOR, $path) . '.php';
        if (!Storage::disk('base')->exists($path)) {
            $preset = $this->getSource('Application');
            $preset = Str::replace('{$domainName}', $domainName, $preset);
            Storage::disk('base')->put($path, $preset);
        }
    }

    private function makeMigration(string $entityName): void
    {
        $migrationName = 'Create' . Str::plural($entityName) . 'Table';
        $this->call('make:migration', ['name' => $migrationName]);
    }

    private function assetModelExists($path): void
    {

    }
}
